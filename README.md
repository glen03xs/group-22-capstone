
# Application Description

Name: Glen Nepomuceno 

This is a the initial setup of the project. The project will use MERN Stack.

## Why MIT licence?
Besides being the one of the most popular, MIT License is optimized for developers and to the point.

## Requirements

1. Text Editor (Visual Studio Code)
2. NodeJS
  
## User instructions

$ git clone https://glen03xs@bitbucket.org/glen03xs/group-22-capstone.git
$ cd group-22-capstone

$ npm install
$ npm start

The application run on  [localhost:5000](http://localhost:4000/).